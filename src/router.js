import gql from "graphql-tag";
import { createRouter, createWebHistory } from "vue-router";
import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client/core'
import Home from './components/Home.vue'
import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import Product from './components/Product.vue'
import Delivery from './components/Delivery.vue'
import BuyProduct from './components/BuyProduct.vue'
import Sales from './components/Sales.vue'
import Inventory from './components/Inventory.vue'
import UpdateUser from './components/UpdateUser.vue'

const routes = [
  {
    path: '/user/logIn',
    name: "logIn",
    component: LogIn,
    meta: { requiresAuth: false, requiresBeStaff: false }
  },
  {
    path: '/user/signUp',
    name: "signUp",
    component: SignUp,
    meta: { requiresAuth: false, requiresBeStaff: false }
  },
  {
    path: '/user/home',
    name: "home",
    component: Home,
    meta: { requiresAuth: true, requiresBeStaff: false }
  },
  {
    path: '/user/product',
    name: "product",
    component: Product,
    meta: { requiresAuth: true, requiresBeStaff: false }
  },
  {
    path: '/user/delivery',
    name: "delivery",
    component: Delivery,
    meta: { requiresAuth: true, requiresBeStaff: false }
  },
  {
    path: '/user/product/buy/:productId',
    name: "buyProduct",
    component: BuyProduct,
    meta: { requiresAuth: true , requiresBeStaff: false}
  },
  {
    path: '/sales',
    name: "sales",
    component: Sales,
    meta: { requiresAuth: true, requiresBeStaff: true }
  },
  {
    path: '/inventory',
    name: "inventory",
    component: Inventory,
    meta: { requiresAuth: true, requiresBeStaff: true }
  },
  {
    path: '/user/updateuser',
    name: "updateuser",
    component: UpdateUser,
    meta: { requiresAuth: true, requiresBeStaff: true }
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

const apolloClient = new ApolloClient({
  link: createHttpLink({ uri: 'https://dream-shoes-api-gateway.herokuapp.com/' }),
  cache: new InMemoryCache()
})

async function isAuth() {
  if (localStorage.getItem("token_access") === null ||
  localStorage.getItem("token_refresh") === null) {
    return false;
  }

  try {
    var result = await apolloClient.mutate({
      mutation: gql `
        mutation ($refresh: String!) {
          refreshToken(refresh: $refresh) {
            access
          }
        }
      `,
      variables: {
        refresh: localStorage.getItem("token_refresh"),
      },
    })
    localStorage.setItem("token_access", result.data.refreshToken.access);
    return true;
  } catch {
    localStorage.clear();
    alert("Su sesión expiró, por favor vuelva a iniciar sesión");
    return false;
  }
}


router.beforeEach(async(to, from) => {
  var is_auth = await isAuth();
  if (is_auth == to.meta.requiresAuth) return true;
  if (is_auth) return { name: "home" };
  return { name: "logIn" };
})

export default router;